package com.epic.demo


interface Callback<T> {

    fun invoke(obj: Void?)
}
