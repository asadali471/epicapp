package com.epic.demo.repo

import com.epic.demo.executors.AppExecutors
import com.epic.demo.models.DataItem
import com.epic.demo.network.BaseRepository
import com.epic.demo.network.NetworkHandler
import com.epic.demo.utils.AppLog
import com.epic.demo.utils.Util
import com.google.gson.JsonArray
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*

/**
 * Created by Muzammil on 19-Mar-19.
 */
open class EpicRepository(
    private val appExecutors: AppExecutors,
    private val mNetworkHandler: NetworkHandler
) : BaseRepository(appExecutors), EpicDataSource {

    init {

    }

    override fun loadEpicImage(callback: EpicDataSource.FetchImageCallback) {

        object : Callback<JsonArray> {
            override fun onResponse(
                call: Call<JsonArray>, response: Response<JsonArray>
            ) {

                try {
                    if (response.isSuccessful && response.body() != null) {
                        val responseBody: JsonArray = response.body()!!
                        AppLog.d(
                            TAG,
                            "loadEpicImage() > onResponse(), code: " + response.code() + ", body: " + responseBody
                        )


                        val imageData = Util.GsonUtils.fromJSON(
                            responseBody[0].toString(),
                            DataItem::class.java
                        )

                        val date = Util.DateUtil.getFormattedDateOrTime(
                            TimeZone.getDefault(),
                            imageData.date,
                            "yyyy-MM-dd HH:mm:ss",
                            "yyyy/MM/dd"
                        )

                        val imagePath =
                            "https://epic.gsfc.nasa.gov/archive/natural/$date/png/${imageData.image!!}.png"

                        callback.onImageLOad(imagePath)
                    } else {
                        AppLog.d(
                            TAG,
                            "loadEpicImage() > onResponse(), code: " + response.code()
                        )

                        callback.onImageLoadFailed("Response parsing failed")
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                    callback.onImageLoadFailed("Something went wrong")
                } catch (e: JSONException) {
                    e.printStackTrace()
                    callback.onImageLoadFailed("Something went wrong")
                } catch (e: NullPointerException) {
                    e.printStackTrace()
                    callback.onImageLoadFailed("Something went wrong")
                }
            }

            override fun onFailure(call: Call<JsonArray>, t: Throwable) {
                AppLog.e(TAG, "loadEpicImage() > onFailure(), error: " + t.localizedMessage)
                callback.onImageLoadFailed(t.localizedMessage)
            }
        }.let {
            mNetworkHandler
                .throughGeneralWebService(it)
                ?.fetchImage()
                ?.enqueue(it)
        }

    }

    /* override fun createWish(saveWish: SaveWish,callback: WishDataSource.CreateWishCallback) {
         var insertCallback = 0
         val runnable = Runnable {
             if (saveWish.id > 0) {
                 insertCallback = appDatabase.wishDao().update(saveWish)
             } else {
                 insertCallback = appDatabase.wishDao().insert(saveWish).toInt()
                 saveWish.id = insertCallback
             }
             appExecutors.mainThread().execute {
                 if (insertCallback > 0) {
                     callback.onInserted(saveWish)
                 }
             }
         }
         appExecutors.diskIO().execute(runnable)
     }*/

    companion object {
        val TAG = EpicRepository::class.java.simpleName
    }
}