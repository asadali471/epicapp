package com.epic.demo.repo

import android.content.Context
import android.content.SharedPreferences

class SharedPref private constructor(context: Context) {
    
    private val mContext: Context
    
    private val mPref: SharedPreferences
    
    init {
        this.mContext = context.applicationContext
        mPref = mContext.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
    }
    
    fun savePref(key: String, value: String) {
        val mEditor = mPref.edit()
        mEditor.putString(key, value)
        mEditor.apply()
    }
    
    fun savePref(key: String, value: Int) {
        val mEditor = mPref.edit()
        mEditor.putInt(key, value)
        mEditor.apply()
    }
    
    fun savePref(key: String, value: Long) {
        val mEditor = mPref.edit()
        mEditor.putLong(key, value)
        mEditor.apply()
    }
    
    fun savePref(key: String, value: Boolean) {
        val mEditor = mPref.edit()
        mEditor.putBoolean(key, value)
        mEditor.apply()
    }
    
    fun getStringPref(key: String, defaultVal: String): String? {
        return mPref.getString(key, defaultVal)
    }
    
    fun getIntPref(key: String, defaultVal: Int): Int {
        try {
            return mPref.getInt(key, defaultVal)
        } catch (e: ClassCastException) {
            return defaultVal
        }
    }
    
    fun getLongPref(key: String, defaultVal: Long): Long {
        return mPref.getLong(key, defaultVal)
    }
    
    fun getBooleanPref(key: String, defaultVal: Boolean): Boolean {
        return mPref.getBoolean(key, defaultVal)
    }
    
    fun clearStoredData() {
        val mEditor = mPref.edit()
        mEditor.clear()
        mEditor.apply()
    }
    
    companion object {
        
        private val PREF_NAME = "AgriSurveyPref"
        
        private val PRIVATE_MODE = 0
        
        fun getInstance(context: Context): SharedPref {
            return SharedPref(context)
        }
    }
}