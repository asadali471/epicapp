package com.epic.demo.repo


/**
 * Created by Muzammil on 19-Mar-19.
 */
interface EpicDataSource {

    fun loadEpicImage(callback: FetchImageCallback)

    interface FetchImageCallback {
        fun onImageLoadFailed(message: String)
        fun onImageLOad(image: String)

    }
}