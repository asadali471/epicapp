package com.epic.demo.models

import com.google.gson.annotations.SerializedName

data class LunarJ2000Position(

    @field:SerializedName("x")
    val X: Double? = null,

    @field:SerializedName("y")
    val Y: Double? = null,

    @field:SerializedName("z")
    val Z: Double? = null
)

data class DataItem(

    @field:SerializedName("date")
    val date: String? = null,

    @field:SerializedName("identifier")
    val identifier: String? = null,

    @field:SerializedName("image")
    val image: String? = null,

    @field:SerializedName("lunar_j2000_position")
    val lunarJ2000Position: LunarJ2000Position? = null,

    @field:SerializedName("attitude_quaternions")
    val attitudeQuaternions: AttitudeQuaternions? = null,

    @field:SerializedName("dscovr_j2000_position")
    val dscovrJ2000Position: DscovrJ2000Position? = null,

    @field:SerializedName("caption")
    val caption: String? = null,

    @field:SerializedName("sun_j2000_position")
    val sunJ2000Position: SunJ2000Position? = null,

    @field:SerializedName("version")
    val version: String? = null,

    @field:SerializedName("coords")
    val coords: Coords? = null,

    @field:SerializedName("centroid_coordinates")
    val centroidCoordinates: CentroidCoordinates? = null
)

data class DscovrJ2000Position(

    @field:SerializedName("x")
    val X: Double? = null,

    @field:SerializedName("y")
    val Y: Double? = null,

    @field:SerializedName("z")
    val Z: Double? = null
)

data class SunJ2000Position(

    @field:SerializedName("x")
    val X: Double? = null,

    @field:SerializedName("y")
    val Y: Double? = null,

    @field:SerializedName("z")
    val Z: Double? = null
)

data class AttitudeQuaternions(

    @field:SerializedName("q1")
    val q1: Double? = null,

    @field:SerializedName("q2")
    val q2: Double? = null,

    @field:SerializedName("q3")
    val q3: Double? = null,

    @field:SerializedName("q0")
    val q0: Double? = null
)

data class CentroidCoordinates(

    @field:SerializedName("lon")
    val lon: Double? = null,

    @field:SerializedName("lat")
    val lat: Double? = null
)

data class Coords(

    @field:SerializedName("lunar_j2000_position")
    val lunarJ2000Position: LunarJ2000Position? = null,

    @field:SerializedName("attitude_quaternions")
    val attitudeQuaternions: AttitudeQuaternions? = null,

    @field:SerializedName("dscovr_j2000_position")
    val dscovrJ2000Position: DscovrJ2000Position? = null,

    @field:SerializedName("sun_j2000_position")
    val sunJ2000Position: SunJ2000Position? = null,

    @field:SerializedName("centroid_coordinates")
    val centroidCoordinates: CentroidCoordinates? = null
)
