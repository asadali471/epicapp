package com.daruma.demo.models

import com.google.gson.annotations.SerializedName

abstract class BaseApiResponse<T> (
    @SerializedName("data") var data: T? = null
)