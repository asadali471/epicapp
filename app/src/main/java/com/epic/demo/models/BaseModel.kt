package com.epic.demo.models

/**
 * Created by Muzammil on 07-Apr-18.
 */

interface BaseModel {
    
    val uniqueId: String
}
