package com.daruma.demo.executors

import java.util.concurrent.Executor
import java.util.concurrent.Executors

/**
 * Executor that runs a task on a new background thread.
 */
class DiskIOThreadExecutor : Executor {
    
    private val mDiskIO: Executor
    
    init {
        mDiskIO = Executors.newFixedThreadPool(THREAD_COUNT)
    }
    
    override fun execute(command: Runnable) {
        mDiskIO.execute(command)
    }
    
    companion object {
        
        private val THREAD_COUNT = 2
    }
}
