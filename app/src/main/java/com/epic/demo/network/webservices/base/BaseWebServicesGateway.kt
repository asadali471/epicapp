package com.epic.demo.network.webservices.base

import com.epic.demo.network.NetworkHandler
import java.util.*

/**
 * Created by Muzammil on 23-Oct-19.
 */
abstract class BaseWebServicesGateway : WebServicesGateway {
    
    lateinit var networkHandler: NetworkHandler
    private val webServicesHashMap: WeakHashMap<String, Any> = WeakHashMap()
    
    override fun init(networkHandler: NetworkHandler) {
        this.networkHandler = networkHandler
    }

    
    override fun <K> getWebService(className: Class<K>): K {
        return className.simpleName.let { key ->
            webServicesHashMap[key]?.let {
                it as K
            } ?: run {
                networkHandler.retrofitClient.create(className).also { newlyCreatedWebService ->
                    webServicesHashMap.put(key, newlyCreatedWebService)
                }
                
            }
        }
    }
    
}