package com.tween.vax.repo

import okhttp3.ResponseBody
import retrofit2.Response

/**
 * Created by Muzammil on 08-Mar-19.
 */

interface BaseDataSource {
    
    fun handleError(
            response: Response<ResponseBody>, callback: OnErrorHandledCallback): Boolean
    
    
    interface OnErrorHandledCallback {
        fun onErrorHandled()
    }
    
}