package com.tween.vax.network.webservices

import com.epic.demo.network.webservices.GeneralWebService
import com.epic.demo.network.webservices.base.BaseWebServicesGateway
import retrofit2.Callback

/**
 * Created by Muzammil on 23-Oct-19.
 */
class AppWebServicesGateway : BaseWebServicesGateway() {

    override fun <T> throughGeneralWebService(callback: Callback<T>): GeneralWebService? {
        val className = GeneralWebService::class.java
        return if (networkHandler.checkWebServiceAndNetworkConnectivityAndShowMessage(callback)) {
            null
        } else {
            getWebService(className)
        }
    }

}