package com.epic.demo.network.webservices

import com.epic.demo.config.API
import com.google.gson.JsonArray
import retrofit2.Call
import retrofit2.http.GET

interface GeneralWebService {

    @GET(API.FETCH_IMAGE_URL)
    fun fetchImage(): Call<JsonArray>
}