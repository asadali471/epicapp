package com.epic.demo.network

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import com.epic.demo.EpicApp
import dagger.android.DaggerBroadcastReceiver
import javax.inject.Inject

class NetworkChangeReceiver : DaggerBroadcastReceiver() {
    
    @Inject
    lateinit var networkHandler: NetworkHandler
    @Inject
    lateinit var app: EpicApp
    
    override fun onReceive(context: Context, intent: Intent) {
        super.onReceive(context, intent)
        val status = isOnline()
        if (status == true) {
        }
    }
    
    private fun isOnline(): Boolean {
        val connectivityManager =
                app.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }
}