package com.epic.demo.network

import android.content.Context
import android.net.ConnectivityManager
import android.widget.Toast
import com.epic.demo.BuildConfig
import com.epic.demo.R
import com.epic.demo.network.webservices.base.WebServicesGateway
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.*
import java.util.concurrent.TimeUnit
import com.google.gson.GsonBuilder
import okio.Timeout


/**
 * Created by Muzammil on 5/26/2017.
 */

class NetworkHandler(
        private val mContext: Context,
        webServicesGatewayImpl: WebServicesGateway
) : WebServicesGateway by webServicesGatewayImpl {
    internal var client: OkHttpClient?
    internal val retrofitClient: Retrofit
    
    init {
    
        val gson = GsonBuilder().setLenient().create()
        webServicesGatewayImpl.init(this)
        val builder = OkHttpClient.Builder()
        client = builder
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build()
        retrofitClient = Retrofit.Builder().baseUrl(BuildConfig.ApiUrl).client(client)
                .addConverterFactory(GsonConverterFactory.create(gson)).build()
    }
    
    /**
     * Check if there is any connectivity
     */
    private val isOnline: Boolean
        get() {
            val connectivityManager =
                    mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = connectivityManager.activeNetworkInfo
            return networkInfo != null && networkInfo.isAvailable && networkInfo.isConnected
        }
    
    fun provideHeaderMap(): Map<String, String> {
        val headerMap = HashMap<String, String>()
      /*  headerMap["Authorization"] = "Bearer " + userSessionRepo.getUserAccessToken()
        headerMap["Accept"] = "application/json"
        headerMap["Content-Type"] = "application/json"
        headerMap["User-Agent"] = BuildConfig.APPLICATION_ID + "_" + BuildConfig.VERSION_NAME*/
        return headerMap
    }
    
    fun <T> checkWebServiceAndNetworkConnectivityAndShowMessage(callback: Callback<T>): Boolean {
        
        if (!isOnline) {
            Toast.makeText(mContext, R.string.network_not_available, Toast.LENGTH_SHORT).show()
            callback.onFailure(EmptyCall(), NetworkNotAvailable(mContext))
            return true
        }
        return false
    }
    
    class NetworkNotAvailable internal constructor(context: Context) :
            Throwable(context.getString(R.string.network_not_available))
    
    private class EmptyCall<T> : Call<T> {
        @Throws(IOException::class)
        override fun execute(): Response<T>? {
            return null
        }
        
        override fun enqueue(callback: Callback<T>) {
        
        }
        
        override fun isExecuted(): Boolean {
            return false
        }
        
        override fun cancel() {
        
        }
        
        override fun isCanceled(): Boolean {
            return false
        }
        
        override fun clone(): Call<T> {
            return EmptyCall()
        }
        
        override fun request(): Request? {
            return null
        }

        override fun timeout(): Timeout {
            return Timeout()
        }
    }
    
    companion object {
        private val TAG = NetworkHandler::class.java.simpleName
    }
}
