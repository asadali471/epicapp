package com.epic.demo.network

import com.daruma.demo.models.ErrorObject
import com.epic.demo.executors.AppExecutors
import com.epic.demo.utils.AppLog
import com.epic.demo.utils.Util
import com.google.gson.JsonSyntaxException
import com.tween.vax.repo.BaseDataSource
import okhttp3.ResponseBody
import org.json.JSONException
import retrofit2.Response
import java.io.IOException


abstract class BaseRepository(
        private val appExecutors: AppExecutors
) : BaseDataSource {
    
    protected fun <T> handleBasicResponse(
            tag: String,
            calledFrom: String? = "Unknown Procedure",
            response: Response<T>,
            successCallback: (response: T) -> Unit,
            failureCallback: (message: String) -> Unit) {
        try {
            val responseBody = response.body()
            if (response.isSuccessful && responseBody != null) {
                AppLog.d(tag,
                         "$calledFrom > onResponse(), code: " + response.code() + ", body: " + responseBody)
                successCallback(responseBody)
            } else {
                val message = response.errorBody()?.string()?.let { errorBody ->
                    AppLog.d(tag,
                             "$calledFrom > onResponse(), code: " + response.code() + ", errorBody: " + errorBody)
                    val errorObject = Util.GsonUtils.fromJSON(errorBody, ErrorObject::class.java)
                    errorObject.errors?.values?.iterator()
                            ?.takeIf { it.hasNext() }
                            ?.next()
                    ?: errorObject.message ?: "Error message is NULL"
                } ?: "Error body is NULL"
                failureCallback(message)
            }
        } catch (e: IOException) {
            e.printStackTrace()
            failureCallback("Response parsing failed")
        } catch (e: JsonSyntaxException) {
            e.printStackTrace()
            failureCallback("Response parsing failed")
        } catch (e: JSONException) {
            e.printStackTrace()
            failureCallback("Response parsing failed")
        }
    }
    
    override fun handleError(
            response: Response<ResponseBody>,
            callback: BaseDataSource.OnErrorHandledCallback
    ): Boolean {
        TODO("Not implemented")
    }
    
    companion object {
        private val TAG = BaseRepository::class.java.simpleName
    }
    
}