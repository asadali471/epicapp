package com.epic.demo.network;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.BufferedSink;

/**
 * Created by Amir Raza on 5/26/2017.
 */

public class ProgressRequestBody extends RequestBody {

    private static final String TAG = ProgressRequestBody.class.getSimpleName();

    private static final int DEFAULT_BUFFER_SIZE = 2048;

    private File mFile;

    private MediaType mMediaType;

    private UploadCallback mListener;

    public ProgressRequestBody(MediaType mediaType, final File file, final UploadCallback listener) {
        mMediaType = mediaType;
        mFile = file;
        mListener = listener;
    }

    @Override
    public MediaType contentType() {
        return mMediaType;
    }

    @Override
    public long contentLength() throws IOException {
        return mFile.length();
    }

    @Override
    public void writeTo(BufferedSink sink) throws IOException {
        long fileLength = mFile.length();
        byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
        FileInputStream in = new FileInputStream(mFile);
        long uploaded = 0;

        long mb = 10 * 1024 * 1024;
        long interval = 0;

        try {
            int read;
            Handler handler = new Handler(Looper.getMainLooper());
            while ((read = in.read(buffer)) != -1) {

                uploaded += read;
                sink.write(buffer, 0, read);

                interval += read;
                if (interval > mb) {
                    Log.i(TAG, "writeTo: " + uploaded);
                    interval = 0;
                }

                // update progress on UI thread
                handler.post(new ProgressUpdater(uploaded, fileLength));
            }
        } finally {
            in.close();
        }
    }

    public interface UploadCallback {

        void onProgressUpdate(int percentage, long uploaded, long total);

        void onError();

        void onFinish();
    }

    private class ProgressUpdater implements Runnable {

        private long mUploaded;

        private long mTotal;

        public ProgressUpdater(long uploaded, long total) {
            mUploaded = uploaded;
            mTotal = total;
        }

        @Override
        public void run() {
            mListener.onProgressUpdate((int) (100 * mUploaded / mTotal), mUploaded, mTotal);
        }
    }
}
