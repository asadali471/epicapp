package com.epic.demo.network.webservices.base

import com.epic.demo.network.webservices.GeneralWebService
import com.epic.demo.network.NetworkHandler
import retrofit2.Callback

/**
 * Created by Muzammil on 23-Oct-19.
 */
interface WebServicesGateway {

    fun <T> throughGeneralWebService(callback: Callback<T>): GeneralWebService?
    
    fun init(networkHandler: NetworkHandler)
    
    fun <K> getWebService(className: Class<K>): K
}