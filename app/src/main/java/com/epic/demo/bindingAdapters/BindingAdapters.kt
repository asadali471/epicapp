package com.epic.demo.bindingAdapters

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.MultiTransformation
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions


object BindingAdapters {
    @BindingAdapter(value = ["imageUrl", "placeHolder", "isRoundEdges"], requireAll = false)
    @JvmStatic fun loadImage(
            imageView: ImageView,
            url: String?,
            placeholder: Drawable,
            isRoundEdges: Boolean) {
        url?.let {
            if (isRoundEdges) {
                Glide.with(imageView.context).load(it)
                        .apply(RequestOptions.bitmapTransform(MultiTransformation(CenterCrop(),
                                RoundedCorners(15))).placeholder(placeholder).error(placeholder))
                        .into(imageView)
            } else {
                Glide.with(imageView.context).load(it)
                        .apply(RequestOptions().placeholder(placeholder).error(placeholder))
                        .into(imageView)
            }
        }
    }
}
