package com.epic.demo

import dagger.android.DaggerApplication

abstract class BaseApplication : DaggerApplication() {

    fun onBaseApplicationCreate(): Boolean {
        return true
    }
    
    companion object {
        
        private val TAG = BaseApplication::class.java.simpleName
    }
}