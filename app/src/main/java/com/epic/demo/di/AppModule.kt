package com.epic.demo.di

import com.epic.demo.repo.EpicDataSource
import com.epic.demo.repo.EpicRepository
import com.epic.demo.EpicApp
import com.epic.demo.executors.AppExecutors
import com.epic.demo.handler.NotificationHandler
import com.epic.demo.repo.SharedPref
import com.epic.demo.network.NetworkHandler
import com.epic.demo.network.webservices.base.WebServicesGateway
import com.tween.vax.network.webservices.AppWebServicesGateway
import dagger.Binds
import dagger.Module
import dagger.Provides
import org.greenrobot.eventbus.EventBus
import javax.inject.Singleton

/**
 * Modules are a way of telling Dagger how to provide dependencies from the dependency graph. These are typically
 * high level dependencies that you are not already contributing to the dependency graph through the @Inject constructor
 * annotation.
 *
 * Note:
 * If we want to use @method [Binds] and @method [Provides] in the same @method [Module], we have to define interface i.e BindsModule
 * and use @method [Binds] inside the BindsModule and also we have to include our interface in the parent module in this case its
 * @see AppModule
 * */


@Module class AppModule {

   /* @Singleton
    @Provides
    fun provideAppDatabase(app: DarumaApp): AppDatabase {
        return Room.databaseBuilder(app, AppDatabase::class.java, "daruma-db.db")
            .fallbackToDestructiveMigration()
            .build()
    }
*/
    @Singleton @Provides fun provideAppExecutors(): AppExecutors {
        return AppExecutors()
    }
    
    @Singleton @Provides fun provideNotificationHandler(): NotificationHandler {
        return NotificationHandler()
    }
    
    @Singleton @Provides fun provideEventBus(): EventBus {
        return EventBus.getDefault()
    }
    
    @Provides fun provideSharedPreferences(app: EpicApp): SharedPref {
        return SharedPref.getInstance(app)
    }

    @Singleton @Provides fun provideWebServicesGatewayImpl(): WebServicesGateway {
        return AppWebServicesGateway()
    }

    @Singleton @Provides fun provideNetworkHandler(
        app: EpicApp,
        webServicesGateway: WebServicesGateway
    ): NetworkHandler {
        return NetworkHandler(app, webServicesGateway)
    }

    @Singleton @Provides
    fun provideWishRepo(
        appExecutors: AppExecutors,
        mNetworkHandler: NetworkHandler
    ): EpicDataSource {
        return EpicRepository(appExecutors,mNetworkHandler)
    }
}