package com.epic.demo.di

import com.epic.demo.EpicApp
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Components are essentially the glue that holds everything together. They are a way of telling Dagger 2 what
 * dependencies should be bundled together and made available to a given instance so they can be used. */
@Singleton
@Component(modules = [ActivityBindingModule::class, AppModule::class, AndroidInjectionModule::class, AndroidSupportInjectionModule::class])
interface AppComponent : AndroidInjector<EpicApp> {
    @Component.Builder interface Builder {
        @BindsInstance fun application(application: EpicApp): Builder
        
        fun build(): AppComponent
    }
    
}