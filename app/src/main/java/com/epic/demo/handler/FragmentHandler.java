package com.epic.demo.handler;

import androidx.annotation.AnimRes;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.epic.demo.R;
import com.epic.demo.ui.base.BaseActivity;
import com.epic.demo.utils.Util;

/**
 * Created by Muhammad Muzammil on 11/28/2016.
 */

public class FragmentHandler {

    /*
     * Replace Fragments with default animations
     * */
    public static void replaceFragment(FragmentActivity context, Fragment fragment, int
            resId, boolean addToBackStack, boolean shouldCommitNow) {
        FragmentHandler.replaceFragmentWithoutAnimation(context, fragment, resId,
                addToBackStack, fragment.getClass().getSimpleName(), shouldCommitNow);
    }

    public static void replaceFragmentWithAnimation(FragmentActivity context, Fragment fragment, int resId, boolean addToBackStack, String backStackEntryName, @AnimRes int enter,
                                                    @AnimRes int exit, @AnimRes int popEnter, @AnimRes int popExit, boolean shouldCommitNow) {
        FragmentTransaction fragmentTransaction = context.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(enter, exit, popEnter, popExit);
        fragmentTransaction.replace(resId, fragment, fragment.getClass().getSimpleName());
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(backStackEntryName);
        }
        commitTransaction(context, fragmentTransaction);
    }

    public static void commitTransaction(FragmentActivity context, FragmentTransaction transaction) {
        if (isOnPauseCalled(context)) return;

        hideKeyboard(context);
        transaction.commitAllowingStateLoss();
    }

    public static void commitNowTransaction(FragmentActivity context, FragmentTransaction transaction) {
        if (isOnPauseCalled(context)) return;

        hideKeyboard(context);
        transaction.commitNowAllowingStateLoss();
    }

    private static boolean isOnPauseCalled(FragmentActivity context) {
        if (context instanceof BaseActivity) {
            return ((BaseActivity) context).isOnPauseCalled();
        }
        return false;
    }

    //To hide keyboard
    private static void hideKeyboard(FragmentActivity context) {
        Util.hideKeyboard(context);
    }

    /*
     * Add Fragments with default animations
     * */
    public static void addFragment(FragmentActivity context, Fragment fragment, int resId, boolean addToBackStack, boolean shouldCommitNow) {
        FragmentHandler.addFragmentWithAnimation(context, fragment, resId, addToBackStack, fragment.getClass().getSimpleName(), R.anim.fade_in,
                R.anim.fade_out, R.anim.fade_in, R.anim.fade_out, shouldCommitNow);
    }

    public static void addFragmentWithAnimation(FragmentActivity context, Fragment fragment,
                                                int resId, boolean addToBackStack,
                                                String backStackEntryName, @AnimRes int enter,
                                                @AnimRes int exit, @AnimRes int popEnter, @AnimRes int popExit, boolean shouldCommitNow) {
        FragmentTransaction fragmentTransaction = context.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(enter, exit, popEnter, popExit);
        fragmentTransaction.add(resId, fragment, fragment.getClass().getSimpleName());
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(backStackEntryName);
        }
        if (shouldCommitNow) {
            commitNowTransaction(context, fragmentTransaction);
        } else {
            commitTransaction(context, fragmentTransaction);
        }
    }


	/*public static void addFragmentWithBottomUpAnim(FragmentActivity context, Fragment fragment,
												int resId, boolean addToBackStack,
												String backStackEntryName, @AnimRes int enter,
												@AnimRes int exit) {
		FragmentTransaction fragmentTransaction = context.getSupportFragmentManager().beginTransaction();
		fragmentTransaction.setCustomAnimations(R.anim.in_from_bottom, R.anim.out_to_bottom, R.anim.in_from_bottom, R.anim.out_to_bottom);
		fragmentTransaction.add(resId, fragment, fragment.getClass().getSimpleName());
		if (addToBackStack) {
			fragmentTransaction.addToBackStack(backStackEntryName);
		}
		commitTransaction(context, fragmentTransaction);
	}*/

    public static void replaceFragmentWithoutAnimation(FragmentActivity context, Fragment fragment,
                                                       int resId, boolean addToBackStack,
                                                       String backStackEntryName, boolean shouldCommitNow) {
        FragmentTransaction fragmentTransaction = context.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(resId, fragment, fragment.getClass().getSimpleName());
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(backStackEntryName);
        }
        if (shouldCommitNow) {
            commitNowTransaction(context, fragmentTransaction);
        } else {
            commitTransaction(context, fragmentTransaction);
        }
    }

    public static void addFragmentWithoutAnimation(FragmentActivity context, Fragment fragment, int
            resId, boolean addToBackStack, String backStackEntryName, boolean shouldCommitNow) {
        FragmentTransaction fragmentTransaction = context.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(resId, fragment, fragment.getClass().getSimpleName());
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(backStackEntryName);
        }
        commitTransaction(context, fragmentTransaction);
    }

    public static void popAllFragments(FragmentActivity context) {
        if (isOnPauseCalled(context)) return;

        hideKeyboard(context);

        FragmentManager fragmentManager = context.getSupportFragmentManager();
        for (int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
            fragmentManager.popBackStack();
        }
    }

    public static void popTransactionUntilName(FragmentActivity context, String name, boolean isInclusive) {
        if (isOnPauseCalled(context)) return;

        hideKeyboard(context);

        FragmentManager fragmentManager = context.getSupportFragmentManager();
        fragmentManager.popBackStack(name, isInclusive ? FragmentManager.POP_BACK_STACK_INCLUSIVE : 0);
    }

    public static Fragment getCurrentFragment(FragmentActivity context) {
        FragmentManager fragmentManager = context.getSupportFragmentManager();
        String fragmentTag = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1).getName();
        Fragment currentFragment = fragmentManager.findFragmentByTag(fragmentTag);
        return currentFragment;
    }

    public static Fragment getBackStackEntryByName(@NonNull FragmentActivity context, @NonNull String name) {
        FragmentManager fragmentManager = context.getSupportFragmentManager();
        String fragmentTag = null;
        for (int i = 0; i < fragmentManager.getBackStackEntryCount(); i++) {
            fragmentTag = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1).getName();
            if (fragmentTag.equals(name)) break;
        }
        if (fragmentTag == null) return null;
        return fragmentManager.findFragmentByTag(fragmentTag);
    }

}
