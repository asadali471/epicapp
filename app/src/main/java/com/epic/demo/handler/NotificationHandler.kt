package com.epic.demo.handler

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.epic.demo.BuildConfig
import com.epic.demo.R
import com.epic.demo.repo.SharedPref

class NotificationHandler {
    
    private var mRequestCode: Int = 0
    
    fun sendNotification(context: Context, title: String, message: String, basicIntent: Intent) {
        sendNotification(context, null, message, 0, "", basicIntent, true)
    }
    
    fun sendNotification(
            context: Context,
            title: String,
            message: String,
            basicIntent: Intent,
            tag: String) {
        sendNotification(context, title, message, 0, tag, basicIntent, true)
    }
    
    fun sendNotification(
            context: Context,
            title: String?,
            message: String,
            notificationId: Int,
            tag: String,
            basicIntent: Intent,
            shouldCreateNewNotification: Boolean) {
        mRequestCode = (System.currentTimeMillis() and 0xfffffff).toInt()
        Log.i(TAG, "mRequestCode (request code for pending intent): $mRequestCode")
        val pendingIntent = PendingIntent
                .getActivity(context, mRequestCode, basicIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        val mNotificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        initChannels(context, mNotificationManager)
        
        val notificationBuilder = NotificationCompat.Builder(context, BuildConfig.APPLICATION_ID)
              //  .setSmallIcon(R.drawable.ic_notification)
                .setLargeIcon(BitmapFactory.decodeResource(context.resources, R.mipmap.ic_launcher))
                .setContentTitle(title ?: context.getString(R.string.app_name))
                .setContentText(message).setOnlyAlertOnce(true).setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_SOUND or Notification.DEFAULT_VIBRATE)
                .setContentIntent(pendingIntent).setChannelId(BuildConfig.APPLICATION_ID)
        
        if (shouldCreateNewNotification) {
            if (notificationId != 0) {
                Log.i(TAG, "NotificationId: $notificationId")
                mNotificationManager.notify(tag, notificationId, notificationBuilder.build())
            } else {
                val generatedNotificationId = getNotificationId(context)
                Log.i(TAG, "NotificationId: $generatedNotificationId")
                mNotificationManager
                        .notify(tag, generatedNotificationId, notificationBuilder.build())
            }
        } else {
            Log.i(TAG, "NotificationId: $notificationId")
            mNotificationManager.notify(tag, notificationId, notificationBuilder.build())
        }
    }
    
    private fun initChannels(context: Context, notificationManager: NotificationManager) {
        if (Build.VERSION.SDK_INT < 26) {
            return
        }
        
        val channel = NotificationChannel(BuildConfig.APPLICATION_ID,
                context.getString(R.string.app_name),
                NotificationManager.IMPORTANCE_DEFAULT)
        channel.description = context.getString(R.string.app_name) + " Notification Channel"
        notificationManager.createNotificationChannel(channel)
    }
    
    private fun getNotificationId(context: Context): Int {
        val PREFERENCE_LAST_NOTIF_ID = "PREFERENCE_LAST_NOTIF_ID"
        var id = SharedPref.getInstance(context).getIntPref(PREFERENCE_LAST_NOTIF_ID, 1) + 1
        if (id == Integer.MAX_VALUE / 2) {   // isn't this over kill ??? hahaha!!
            id = 0
        }
        SharedPref.getInstance(context).savePref(PREFERENCE_LAST_NOTIF_ID, id)
        return id
    }
    
    fun sendNotification(
            context: Context,
            message: String,
            basicIntent: Intent,
            shouldCreateNewNotification: Boolean) {
        sendNotification(context, null, message, 0, "", basicIntent, shouldCreateNewNotification)
    }
    companion object {
        
        val instance = NotificationHandler()
        
        private val TAG = NotificationHandler::class.java.simpleName
    }
}
