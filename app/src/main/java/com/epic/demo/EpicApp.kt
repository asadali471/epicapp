package com.epic.demo


import com.epic.demo.di.DaggerAppComponent
import dagger.android.AndroidInjector


class EpicApp : BaseApplication() {
    
    public override fun applicationInjector(): AndroidInjector<out EpicApp> {
        return DaggerAppComponent.builder().application(this).build()
    }
    
    override fun onCreate() {
        super.onCreate()
        instance = this
        init()
    }
    
    private fun init() {

    }
    
    companion object {
        val TAG: String = EpicApp::class.java.simpleName
        lateinit var instance: EpicApp
            private set
    }
    
}
