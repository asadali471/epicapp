package com.epic.demo.config


/**
 * Created by Muzammil on 1/30/2019.
 */
object Constant {

    const val DARUMA_PURCHASED = "daruma_purchased"

    const val NOTIFICATION_RECEIVED = "notification_received"

    const val CLICK_TIME_INTERVAL: Long = 1000

    const val SKU_PURCHASE_DARUMAS = "get_em_pro_test"
    const val BASE_64_ENCODED_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkZXaSNFPq0CNQ2O9elKMqBGrQiiqnRWg4YTeIu3tzDA/Z0QJu2JSCj9ge7rzWKtzDAt029ZstgCLwzOouTiosmPhMHrFzy1QW57SjLAcE58W2efkrkhEpLVAt+dFehRG0ShXu/e9rJR6DL3xiQnkSOf4g0LOMn1boA8u+48Hos3Gvz331rcz7/2qXlxcQyKhAjKdHMsoRHAnSN2DfbWZ28ebaLqm+54vFtCIxrA9IZXIprJGT+W/Itnl2ZuuN5U92oTgtiG6tfL4EaZH9V//rN14B1rkWX+V1XEx9RuTzXhiw89/Z0jejRUJXu45fAGR210LpNs9R6Lfh2BomYKMMwIDAQAB"

}
