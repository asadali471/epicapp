package com.epic.demo.customviews

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.viewpager.widget.ViewPager

/**
 * Created by Muzammil on 10-Sep-18.
 */
class NonSwipeAbleViewPager(context: Context, attrs: AttributeSet) : ViewPager(context, attrs) {
    
    private var enabledSwiping: Boolean = false
    
    init {
        this.enabledSwiping = true
    }
    
    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        return if (this.enabledSwiping) {
            super.onInterceptTouchEvent(event)
        } else false
        
    }
    
    override fun onTouchEvent(event: MotionEvent): Boolean {
        return if (this.enabledSwiping) {
            super.onTouchEvent(event)
        } else false
        
    }
    
    fun setPagingEnabled(enabledSwiping: Boolean) {
        this.enabledSwiping = enabledSwiping
    }
}
