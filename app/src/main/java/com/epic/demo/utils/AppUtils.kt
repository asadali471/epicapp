package com.epic.demo.utils

import com.epic.demo.config.Constant.DARUMA_PURCHASED
import com.epic.demo.repo.SharedPref


object AppUtils {


    //1 purchased
    //"2" pending
    //3 not buy
    fun shouldShowDarumas(myPref: SharedPref): Boolean {
        return myPref.getStringPref(DARUMA_PURCHASED,"0") == "1"
    }

    fun updateInAppPurchase(myPref: SharedPref, isBuy: String) {
        myPref.savePref(DARUMA_PURCHASED,isBuy)
    }

    fun getDarumaStatus(myPref: SharedPref): String {
        return myPref.getStringPref(DARUMA_PURCHASED,"0") ?: "0"
    }

}