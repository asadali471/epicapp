package com.epic.demo.utils

import android.view.View


/**
 * Created by Muzammil on 22-Oct-19.
 */

const val CLICK_TIME_INTERVAL: Long = 1000

fun View.shouldAllowTheClick(): Boolean {
    val lastClickTime = this.tag
    val now = System.currentTimeMillis()
    lastClickTime?.let {
        if (lastClickTime is Long) {
            if (now - lastClickTime < CLICK_TIME_INTERVAL) {
                return false
            }
            this.tag = now
        }
    } ?: run {
        this.tag = now
    }
    return true
}