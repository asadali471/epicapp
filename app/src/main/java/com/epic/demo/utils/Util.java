package com.epic.demo.utils;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.epic.demo.ui.base.BaseActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.math.RoundingMode;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;

public class Util {
	
	private static final String TAG = Util.class.getSimpleName();

	public static int getImageId(Context context,String imageName) {
		return context.getResources()
				.getIdentifier("drawable/"+imageName, null,context.getPackageName());
	}

	@NonNull
	public static String processText(EditText et, CharSequence s) {
		String text = "";
		Object tag = et.getTag() != null ? et.getTag() : "";
		if (tag instanceof String) {
			String oldSequence = (String) tag;
			if (s.length() > oldSequence.length()) {
				if (s.charAt(0) == '0') {
					text = s.toString();
				} else {
					text = "";
				}
			} else {
				text = "";
			}
			et.setTag(text);
		}
		return text;
	}
	
	public static CharSequence trimTrailingWhitespace(CharSequence source) {
		if (source == null) return "";
		
		int i = source.length();
		
		// loop back to the first non-whitespace character
		while (--i >= 0 && Character.isWhitespace(source.charAt(i))) {
		}
		
		return source.subSequence(0, i + 1);
	}
	
	public static String formatNumber(final CharSequence s) {
		StringBuilder sb = new StringBuilder();
		boolean isNumberWithACode = false;
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (i == 0 && c == '+') {
				isNumberWithACode = true;
			}
			if (c != ' ') {
				sb.append(c);
			}
			if (isNumberWithACode) {
				if (i == 2 || i == 7) {
					sb.append(' ');
				}
			} else {
				if (i == 3) {
					sb.append(' ');
				}
			}
		}
		
		return sb.toString();
	}
	
	public static void openMap(Context context, double lat, double lng, String title) {
		String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%f,%f (%s)",
				lat, lng, title);
		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
		intent.setPackage("com.google.android.apps.maps");
		try {
			context.startActivity(intent);
		} catch (ActivityNotFoundException ex) {
			try {
				Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
				context.startActivity(unrestrictedIntent);
			} catch (ActivityNotFoundException innerEx) {
				Toast.makeText(context, "Please install a maps application", Toast.LENGTH_LONG).show();
			}
		}
	}

	public static void showShareMenu(Context context, String message) {
		Intent share = new Intent(Intent.ACTION_SEND);
		share.setType("text/plain");/*	public static void getKeyHash(Context context) {
		PackageInfo info;
		try {
			info = context.getPackageManager().getPackageInfo(BuildConfig.APPLICATION_ID, PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md;
				md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				String something = new String(Base64.encode(md.digest(), 0));
				//String something = new String(Base64.encodeBytes(md.digest()));
			//	AppLog.e(TAG, "hash key: " + something);
			}
		} catch (PackageManager.NameNotFoundException e1) {
			Log.e("name not found", e1.toString());
		} catch (NoSuchAlgorithmException e) {
			Log.e("no such an algorithm", e.toString());
		} catch (Exception e) {
			Log.e("exception", e.toString());
		}
	}*/
		share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
		share.putExtra(Intent.EXTRA_TEXT, message);
		context.startActivity(Intent.createChooser(share, "Share link"));
	}

	public static double distance(double lat1, double lon1, double lat2, double lon2) {
		double theta = lon1 - lon2;
		double dist = Math.sin(deg2rad(lat1))
				* Math.sin(deg2rad(lat2))
				+ Math.cos(deg2rad(lat1))
				* Math.cos(deg2rad(lat2))
				* Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;
		return (dist);
	}

	private static double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	private static double rad2deg(double rad) {
		return (rad * 180.0 / Math.PI);
	}
	
/*	public static void getKeyHash(Context context) {
		PackageInfo info;
		try {
			info = context.getPackageManager().getPackageInfo(BuildConfig.APPLICATION_ID, PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md;
				md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				String something = new String(Base64.encode(md.digest(), 0));
				//String something = new String(Base64.encodeBytes(md.digest()));
			//	AppLog.e(TAG, "hash key: " + something);
			}
		} catch (PackageManager.NameNotFoundException e1) {
			Log.e("name not found", e1.toString());
		} catch (NoSuchAlgorithmException e) {
			Log.e("no such an algorithm", e.toString());
		} catch (Exception e) {
			Log.e("exception", e.toString());
		}
	}*/

	public static String getDeviceId(Context context) {
		return Settings.Secure.getString(context.getContentResolver(),
				Settings.Secure.ANDROID_ID);
	}
	
	public static String getFormattedRemainingTime(long remainingTimeInSeconds) {
		int day = (int) TimeUnit.SECONDS.toDays(remainingTimeInSeconds);
		long hours = TimeUnit.SECONDS.toHours(remainingTimeInSeconds) -
				TimeUnit.DAYS.toHours(day);
		long minute = TimeUnit.SECONDS.toMinutes(remainingTimeInSeconds) -
				TimeUnit.DAYS.toMinutes(day) -
				TimeUnit.HOURS.toMinutes(hours);
		long second = TimeUnit.SECONDS.toSeconds(remainingTimeInSeconds) -
				TimeUnit.DAYS.toSeconds(day) -
				TimeUnit.HOURS.toSeconds(hours) -
				TimeUnit.MINUTES.toSeconds(minute);
		return String.format(Locale.US, "%02d:%02d:%02d:%02d", day, hours, minute, second);
	}
	
	public static DateFormat getDefaultLongDateFormat() {
		return new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.US);
	}
	
	public static DateFormat getDashSeperatedLongDateFormat() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
	}

	public static String getFormattedNumber(String number, int maximumFractionDigits) {
		NumberFormat instance = NumberFormat.getInstance();
		instance.setMinimumFractionDigits(0);
		instance.setMaximumFractionDigits(maximumFractionDigits);
		instance.setRoundingMode(RoundingMode.HALF_UP);
		
		try {
			number = instance.format(NumberFormat.getInstance().parse(number));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return number;
	}
	
	public static boolean isNumeric(String str) {
		NumberFormat formatter = NumberFormat.getInstance();
		ParsePosition pos = new ParsePosition(0);
		formatter.parse(str, pos);
		return str.length() == pos.getIndex();
	}
	
	
	public static void showKeyboard(final EditText view, final BaseActivity activity) {
		view.post(new Runnable() {
			@Override
			public void run() {
				view.requestFocus();
				InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
			}
		});
	}
	
	public static void hideKeyboard(Activity activity) {
		InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
		//Find the currently focused view, so we can grab the correct window token from it.
		View view = activity.getCurrentFocus();
		//If no view currently has focus, create a new one, just so we can grab a window token from it
		if (view == null) {
			view = new View(activity);
			view.requestFocus();
		}
		imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}
	
	public static void changeTypeFace(Context context, TextView textView, String textName) {
		Typeface typeface = Typeface.createFromAsset(context.getAssets(), textName);
		textView.setTypeface(typeface);
	}
	
	
	public static JSONObject mapToJson(Map map) {
		try {
			return GsonUtils.toJSON(map);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static JSONObject createJSONObjectFromFile(Context mContext, String abc) throws IOException, JSONException {
		InputStream is = mContext.getAssets().open(abc);
		String jsonTxt = convertStreamToString(is);
		return new JSONObject(jsonTxt);
	}
	
	private static String convertStreamToString(InputStream is) {
		java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
		return s.hasNext() ? s.next() : "";
	}
	
	public static String createStringFromFile(Context mContext, String assetFileName) throws IOException {
		InputStream is = mContext.getAssets().open(assetFileName);
		return convertStreamToString(is);
	}
	
	public static boolean isViewInBounds(View view, int x, int y) {
		Rect outRect = new Rect();
		int[] location = new int[2];
		view.getDrawingRect(outRect);
		view.getLocationOnScreen(location);
		outRect.offset(location[0], location[1]);
		return outRect.contains(x, y);
	}
	
	public static boolean isValidEmailAddress(String email) {
		String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
		java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
		java.util.regex.Matcher m = p.matcher(email);
		return m.matches();
	}
	
	public static boolean isValidPostcode(String postcode) {
		String ePattern = "^[A-Z]{1,2}[0-9R][0-9A-Z]? [0-9][ABD-HJLNP-UW-Z]{2}$";
		java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
		java.util.regex.Matcher m = p.matcher(postcode);
		return m.matches();
	}
	
	public static String capitalizeFirstWordOfEachSentence(String text) {
		int pos = 0;
		boolean capitalize = true;
		StringBuilder sb = new StringBuilder(text);
		while (pos < sb.length()) {
			if (sb.charAt(pos) == '.') {
				capitalize = true;
			} else if (capitalize && !Character.isWhitespace(sb.charAt(pos))) {
				sb.setCharAt(pos, Character.toUpperCase(sb.charAt(pos)));
				capitalize = false;
			}
			pos++;
		}
		return sb.toString();
	}

	public static boolean containsIgnoreCase(String str, String subString) {
		return str.toLowerCase().contains(subString.toLowerCase());
	}
	
	public static void setNotificationCount(TextView textView, int count) {
		if (count <= 0) {
			textView.setVisibility(View.GONE);
		} else {
			textView.setVisibility(View.VISIBLE);
			if (count > 9) {
				textView.setText("9+");
			} else {
				textView.setText(String.valueOf(count));
			}
		}
	}
	
	public static class DeviceDimensionsHelper {
		
		// DeviceDimensionsHelper.getDisplayWidth(context) => (display width in pixels)
		public static int getDisplayWidth(Context context) {
			DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
			return displayMetrics.widthPixels;
		}
		
		// DeviceDimensionsHelper.getDisplayHeight(context) => (display height in pixels)
		public static int getDisplayHeight(Context context) {
			DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
			return displayMetrics.heightPixels;
		}
		
		// DeviceDimensionsHelper.convertDpToPixel(25f, context) => (25dp converted to pixels)
		public static float convertDpToPixel(float dp, Context context) {
			Resources r = context.getResources();
			return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
		}
		
		// DeviceDimensionsHelper.convertPixelsToDp(25f, context) => (25px converted to dp)
		public static float convertPixelsToDp(float px, Context context) {
			Resources r = context.getResources();
			DisplayMetrics metrics = r.getDisplayMetrics();
			float dp = px / (metrics.densityDpi / 160f);
			return dp;
		}
	}
	
	public static class FontsOverride {
		
		public static void setDefaultFont(Context context,
		                                  String staticTypefaceFieldName, String fontAssetName) {
			final Typeface regular = Typeface.createFromAsset(context.getAssets(),
					fontAssetName);
			replaceFont(staticTypefaceFieldName, regular);
		}
		
		protected static void replaceFont(String staticTypefaceFieldName,
		                                  final Typeface newTypeface) {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && staticTypefaceFieldName == "DEFAULT") {
			//	AppLog.d("fontTesting", "in if");
				Map<String, Typeface> newMap = new HashMap<String, Typeface>();
				newMap.put("sans-serif", newTypeface);
				try {
					final Field staticField = Typeface.class
							.getDeclaredField("sSystemFontMap");
					staticField.setAccessible(true);
					staticField.set(null, newMap);
				} catch (NoSuchFieldException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			} else {
			//	AppLog.d("fontTesting", "in else");
				try {
					final Field staticField = Typeface.class
							.getDeclaredField(staticTypefaceFieldName);
					staticField.setAccessible(true);
					staticField.set(null, newTypeface);
				} catch (NoSuchFieldException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static class GsonUtils {
		
		public static String toJSONString(Object obj) {
			Gson gson = new Gson();
			return gson.toJson(obj);
		}
		
		public static <T> JSONObject toJSON(T obj) throws JSONException {
			Gson gson = new Gson();
			return new JSONObject(gson.toJson(obj));
		}
		
		public static <T> T fromJSON(String json, Class<T> classOfT) {
			Gson gson = new Gson();
			return gson.fromJson(json, classOfT);
		}
		
		public static <T> T fromJSON(JSONObject json, Class<T> classOfT) {
			Gson gson = new Gson();
			return gson.fromJson(json.toString(), classOfT);
		}
		
		public static <T> ArrayList<T> fromJSONtoList(String json) {
			Gson gson = new Gson();
			return gson.fromJson(json, new TypeToken<ArrayList<T>>() {
			}.getType());
		}
	}
	
	public static class DateUtil {
		
		public static final String INVALID_DATE = "Invalid Date";
		
		public static final String HOUR = "hour";
		
		public static final String HOURS = "hours";
		
		public static final String MINUTE = "minute";
		
		public static final String MINUTES = "minutes";
		
		public static final String YEAR = "Year";
		
		public static final String YEARS = "Years";
		
		public static final String MONTH = "Month";
		
		public static final String MONTHS = "Months";
		
		public static final String DAY = "Day";
		
		public static final String DAYS = "Days";
		
		public static final String TODAY = "Today";
		
		public static final String LEFT = "left";
		
		public static final String BEFORE = "before";
		
		private static final long HOUR_24 = 1000 * 60 * 60 * 24;
		
		private static final long offset = TimeZone.getDefault().getRawOffset();
		
		private static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		//returns time in milliseconds
		// timestamp can be like 2016-11-09 09:13:52
		public static long parseTimeStamp(String timeStamp) {
			try {
				return dateFormat.parse(timeStamp).getTime();
			} catch (ParseException e) {
				e.printStackTrace();
				return -1;
			}
		}
		
		public static long convertToLocal(long timestamp) {
			return timestamp + offset;
		}
		
		public static long convertToUTC(long timestamp) {
			return timestamp - offset;
		}
		
		public static String getLastSeenTimeString(long lastSeen) {
			GregorianCalendar calendar = new GregorianCalendar();
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			
			long todayZeroTime = calendar.getTimeInMillis();
			
			String s = "";
			SimpleDateFormat format;
			
			if (lastSeen >= todayZeroTime) {
				s += "Today at ";
			} else if (lastSeen >= todayZeroTime - HOUR_24) {
				s += "Yesterday at ";
			} else {
				format = new SimpleDateFormat("MMMM d");
				String x = format.format(new Date(lastSeen));
			//	AppLog.i("lastseen", x);
				s += x + ", ";
			}
			format = new SimpleDateFormat("h:mm a");
			s += format.format(lastSeen);
			
			return s;
		}
		
		/**
		 * get desired date format from the given time string
		 * example formats
		 * "hh:mm a" ->
		 * "hh:mm:ss"
		 * "yyyy-MM-dd hh:mm:ss"
		 * "MM dd, yyyy"
		 *
		 *
		 * @param timeZone
		 * @param timeStr
		 * @param inputFormat  Can be one of the format mentioned above
		 * @param outputFormat Desired format. Can be one of the format mentioned above
		 * @return desired format time string
		 */
		public static String getFormattedDateOrTime(TimeZone timeZone, String timeStr, String inputFormat, String
				outputFormat) {
			
			String formattedDate = timeStr;
			
			DateFormat readFormat = new SimpleDateFormat(inputFormat, Locale.getDefault());
			readFormat.setTimeZone(timeZone);
			
			SimpleDateFormat writeFormat = new SimpleDateFormat(outputFormat, Locale.getDefault());
			
			Date date = null;
			
			try {
				date = readFormat.parse(timeStr);
			} catch (ParseException e) {
			}
			
			DateFormatSymbols symbols = new DateFormatSymbols(Locale.getDefault());
			// OVERRIDE SOME symbols WHILE RETAINING OTHERS
			symbols.setAmPmStrings(new String[]{"am", "pm"});
			writeFormat.setDateFormatSymbols(symbols);
			if (date != null) {
				formattedDate = writeFormat.format(date);
			}
			
			return formattedDate;
		}


		
		public static Calendar get18YearBefore() {
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - 18);
			return calendar;
		}
		
		public static boolean isBefore18Year(Date date) {
			Calendar calendar18YearsAgo = Calendar.getInstance();
			calendar18YearsAgo.set(Calendar.YEAR, calendar18YearsAgo.get(Calendar.YEAR) - 18);
			return date.getTime() <= calendar18YearsAgo.getTimeInMillis();
		}
		
		public static Date convertToDate(TimeZone timeZone, String dateTimeString, String format) {
			SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
			sdf.setTimeZone(timeZone);
			try {
				return sdf.parse(dateTimeString);
			} catch (ParseException e) {
				e.printStackTrace();
				return new Date();
			}
		}
		
		@NonNull
		public static String calculateTimeDifferenceInHoursOrMinutes(String startTime, String endTime) {
			DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			long start = 0;
			long end = 0;
			
			try {
				start = inputFormat.parse(startTime).getTime();
				end = inputFormat.parse(endTime).getTime();
			} catch (ParseException e) {
				e.printStackTrace();
				return INVALID_DATE;
			}
			
			long diff;
			diff = TimeUnit.MILLISECONDS.toHours(end - start);
			String unit = " " + "hours";
			if (diff == 1) {
				unit = " " + "hour";
			}
			if (diff < 1) {
				diff = TimeUnit.MILLISECONDS.toMinutes(end - start);
				if (diff == 1) {
					unit = " " + "minute";
				} else {
					unit = " " + "minutes";
				}
			}
			return String.valueOf(diff) + unit;
		}
		
		public static String isTodayAndIfNotCalculateDays(Context context, long today, long otherday, String concatenatedString) {
			
			String remainingTimeInDaysMonthOrYear = calculateYearsBetween(context, today, otherday);
			
			if (remainingTimeInDaysMonthOrYear.equals("0")) {
				remainingTimeInDaysMonthOrYear = calculateMonthsBetween(context, today, otherday);
			}
			
			if (remainingTimeInDaysMonthOrYear.equals("0")) {
				remainingTimeInDaysMonthOrYear = calculateDaysBetween(context, today, otherday);
			}
			
			// If it is today, donot concatenate "ago", "before" or "left"
			if (!remainingTimeInDaysMonthOrYear.equals(TODAY)) {
				if (concatenatedString.equals(LEFT)
						&& today > otherday) {
					remainingTimeInDaysMonthOrYear += " " + BEFORE;
				} else {
					remainingTimeInDaysMonthOrYear += " " + concatenatedString;
				}
			}
			
			return remainingTimeInDaysMonthOrYear;
		}
		
		public static String calculateYearsBetween(Context context, long today, long otherday) {
			String years = "0";
			long days = 0;
			if (today > otherday) {
				days = TimeUnit.MILLISECONDS.toDays(today - otherday);
			} else {
				days = TimeUnit.MILLISECONDS.toDays(otherday - today);
			}
			
			// Convert days to years
			if (days >= 360) {
				years = Integer.valueOf((int) (days / 360)).toString();
				
				if (Integer.valueOf(years) < 2) {
					years += " " + YEAR;
				} else {
					years += " " + YEARS;
				}
			}
			
			return years;
		}
		
		public static String calculateMonthsBetween(Context context, long today, long otherday) {
			String months = "0";
			long days = 0;
			if (today > otherday) {
				days = TimeUnit.MILLISECONDS.toDays(today - otherday);
			} else {
				days = TimeUnit.MILLISECONDS.toDays(otherday - today);
			}
			
			// Convert days to months
			if (days >= 30) {
				months = Integer.valueOf((int) (days / 30)).toString();
				
				if (Integer.valueOf(months) < 2) {
					months += " " + MONTH;
				} else {
					months += " " + MONTHS;
				}
			}
			
			return months;
		}
		
		public static String calculateDaysBetween(Context context, long today, long otherday) {
			String days = "0";
			if (today > otherday) {
				days = TimeUnit.MILLISECONDS.toDays(today - otherday) + "";
			} else {
				days = TimeUnit.MILLISECONDS.toDays(otherday - today) + "";
			}
			
			if (Integer.valueOf(days) == 0) {
				return TODAY;
			}
			
			if (Integer.valueOf(days) < 2) {
				days += " " + DAY;
			} else {
				days += " " + DAYS;
			}
			return days;
		}
		
		public static String convertToString(TimeZone timeZone, Date date, String format) {
			SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
			sdf.setTimeZone(timeZone);
			
			return sdf.format(date);
		}
	}
	
	public static Date localToUTCDate(Date gmtDate) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
		calendar.setTime(gmtDate);
		Date utcDate = calendar.getTime();
		return utcDate;
	}
}
