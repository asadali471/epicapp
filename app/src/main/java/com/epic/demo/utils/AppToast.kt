package com.epic.demo.utils

import android.content.Context
import android.widget.Toast
import com.epic.demo.BuildConfig

object AppToast {
    
    fun show(context: Context, message: String) {
        if (BuildConfig.DEBUG) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }
    }
}