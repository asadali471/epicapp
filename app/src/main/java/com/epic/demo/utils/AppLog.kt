package com.epic.demo.utils

import android.util.Log
import com.epic.demo.BuildConfig

/**
 * Created by Muhammad Muzammil on 20/06/2016.
 */
@Suppress("unused") object AppLog {
    
    private val SHOULD_LOG = BuildConfig.DEBUG
    
    fun v(tag: String, msg: String) {
        if (SHOULD_LOG) Log.v(tag, msg)
        
    }
    
    fun v(tag: String, msg: String, tr: Throwable?) {
        if (SHOULD_LOG) Log.v(tag, msg, tr)
    }
    
    fun d(tag: String, msg: String) {
        if (SHOULD_LOG) Log.d(tag, msg)
    }
    
    fun d(tag: String, msg: String, tr: Throwable?) {
        if (SHOULD_LOG) Log.d(tag, msg, tr)
    }
    
    fun i(tag: String, msg: String) {
        if (SHOULD_LOG) Log.i(tag, msg)
    }
    
    fun i(tag: String, msg: String, tr: Throwable?) {
        if (SHOULD_LOG) Log.i(tag, msg, tr)
    }
    
    fun w(tag: String, msg: String) {
        if (SHOULD_LOG) Log.w(tag, msg)
    }
    
    fun w(tag: String, msg: String, tr: Throwable?) {
        if (SHOULD_LOG) Log.w(tag, msg, tr)
    }
    
    fun w(tag: String, tr: Throwable?) {
        if (SHOULD_LOG) Log.w(tag, tr)
    }
    
    fun e(tag: String, msg: String) {
        if (SHOULD_LOG) Log.e(tag, msg)
    }
    
    fun e(tag: String, msg: String, tr: Throwable?) {
        if (SHOULD_LOG) Log.e(tag, msg, tr)
    }
}
