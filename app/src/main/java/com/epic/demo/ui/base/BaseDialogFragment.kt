package com.epic.demo.ui.base

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import androidx.fragment.app.DialogFragment
import com.epic.demo.ui.base.BaseActivity
import dagger.android.support.DaggerAppCompatDialogFragment

/**
 * Created by Muzammil on 11/23/2018.
 */
abstract class BaseDialogFragment : DaggerAppCompatDialogFragment() {
    
    lateinit var mBaseActivity: BaseActivity
    
    abstract fun init()
    
    abstract fun initBinding(
            inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_TITLE, 0)
    }
    
    override fun onAttach(context: Context) {
        super.onAttach(context)
        mBaseActivity = context as BaseActivity
    }
    
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        
        val rootView = initBinding(inflater, container, savedInstanceState)
        
        if (arguments != null) {
            // get Arguments here
        }
        
        init()
        
        dialog?.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        /*isCancelable = false*/
        
        return rootView
    }
    
    override fun onStart() {
        super.onStart()
        
        //changes by asad for pageover ads
        dialog!!.window!!
                .setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        // make dialog itself transparent
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // remove background dim
        dialog!!.window!!.setDimAmount(0.8f)
        
        
        //default properties set
        val window = dialog!!.window
        val windowParams = window!!.attributes
        windowParams.dimAmount = 0.8f
        windowParams.flags = windowParams.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND
        window.attributes = windowParams
    }
}
