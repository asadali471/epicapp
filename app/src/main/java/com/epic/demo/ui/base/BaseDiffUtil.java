package com.epic.demo.ui.base;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;

import com.epic.demo.models.BaseModel;

import java.util.List;

/**
 * Created by Muzammil on 07-Apr-18.
 */

public class BaseDiffUtil<T extends BaseModel> extends DiffUtil.Callback {

    private List<T> oldList;

    private List<T> newList;

    public BaseDiffUtil(List<T> oldList, List<T> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).getUniqueId().equals(newList.get(newItemPosition).getUniqueId());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).equals(newList.get(newItemPosition));
    }

    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        //you can return particular field for changed item.
        return super.getChangePayload(oldItemPosition, newItemPosition);
    }

}
