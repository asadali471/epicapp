package com.tween.vax.ui.dialogs

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.epic.demo.R
import com.epic.demo.databinding.DialogLayoutBinding
import com.epic.demo.ui.base.BaseDialogFragment
import javax.inject.Inject

class DefaultAlertDialog @Inject constructor() : BaseDialogFragment() {
    
    private lateinit var binding: DialogLayoutBinding
    
    private var listener: CustomDialogListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }
    
    
    fun setListener(listener: CustomDialogListener?) {
        this.listener = listener
    }
    
    override fun initBinding(
            inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_layout, container, false)
        
        return binding.root
    }
    
    override fun init() {

        dialog!!.setCancelable(false)
        dialog!!.setCanceledOnTouchOutside(false)
      /*  if (!isDismissible) {
            dialog!!.setOnKeyListener { _, keyCode, event ->
                if ((keyCode == KeyEvent.KEYCODE_BACK)) {
                    if (event?.action != KeyEvent.ACTION_DOWN) true
                    else {
                        Util.hideKeyboard(activity)
                        true
                    }
                } else false
            }
        }*/

        
        setUpListener()
    }
    
    private fun setUpListener() {
        binding.tvOk.setOnClickListener {
            listener?.onOkClicked()
            dismiss()

        }
        
        binding.tvDelete.setOnClickListener {
            listener?.onDeleteClicked()
            dismiss()
        }
    }
    
    interface CustomDialogListener {
        fun onOkClicked()
        fun onDeleteClicked()
    }
    
    override fun onDestroyView() {
        super.onDestroyView()
    }

    
    companion object {
        
        fun newInstance(
                listener: CustomDialogListener?): DefaultAlertDialog {
            val args = Bundle()
            val fragment = DefaultAlertDialog()
            fragment.arguments = args
            fragment.setListener(listener)
            return fragment
        }
    }
    
    
}
