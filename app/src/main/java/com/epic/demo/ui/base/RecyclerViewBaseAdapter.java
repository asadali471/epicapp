package com.epic.demo.ui.base;

import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.epic.demo.config.Constant;
import com.epic.demo.executors.AppExecutors;
import com.epic.demo.models.BaseModel;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

public abstract class RecyclerViewBaseAdapter<T extends BaseModel, V extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<V> {

    protected List<T> list;
    @Nullable
    protected ItemSelectedListener<T> itemSelectedListener;
    boolean operationPending;
    private Deque<List<T>> pendingUpdates =
            new ArrayDeque<>();
    private List<T> newList;
    private AppExecutors appExecutors;

    public RecyclerViewBaseAdapter(List<T> items, @Nullable ItemSelectedListener<T> itemSelectedListener, AppExecutors appExecutors) {
        list = new ArrayList<>(items);
        this.itemSelectedListener = itemSelectedListener;
        this.appExecutors = appExecutors;
    }

    public T getItem(int position) {
        int size = list.size();
        boolean isOk = position >= 0 && position < size;
        return isOk ? list.get(position) : null;
    }

    public void addItem(T item) {
        list.add(item);
        notifyItemInserted(list.size() - 1);
    }

    public void addItem(int position, T item) {
        list.add(position, item);
        notifyItemInserted(position);
    }

    public void addItem(List<T> list) {
        addItem(getItemCount(), list);
    }

    public void addItem(int position, List<T> list) {
        this.list.addAll(position, list);
        notifyItemRangeInserted(position, list.size());
    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    public List<T> getList() {
        return list;
    }

    public int updateItem(T updateItem) {
        for (int i = 0; i < list.size(); i++) {
            T item = list.get(i);
            if (item.equals(updateItem)) {
                list.remove(i);
                list.add(i, updateItem);
                notifyItemChanged(i);
                return i;
            }
        }
        return -1;
    }

    public void clearList() {
        list.clear();
        newList.clear();
        notifyDataSetChanged();
    }

    public void moveToFirst(T item) {
        int pos = getPosition(item);
        if (pos != -1) {
            list.remove(pos);
        }
        list.add(0, item);
        if (pos != -1) {
            notifyItemMoved(pos, 0);
            notifyItemChanged(0);
        } else {
            notifyItemInserted(0);
        }
    }

    public int getPosition(T item) {
        if (list.size() == 0) return -1;

        int i = 0;
        while (i < list.size() && !list.get(i).equals(item)) {
            i++;
        }

        return i >= list.size() ? -1 : i;
    }

    public int removeItem(T item) {
        int pos = getPosition(item);
        if (pos != -1) {
            list.remove(pos);
            notifyItemRemoved(pos);
        }
        return pos;
    }

    public T removeItem(int pos) {
        if (pos != -1) {
            T remove = list.remove(pos);
            notifyItemRemoved(pos);
            return remove;
        }
        return null;
    }


    public void updateList(final List<T> newList) {
        this.newList = new ArrayList<>(newList);
        pendingUpdates.add(newList);
        if (pendingUpdates.size() > 1) {
            return;
        }
        updateListInternal(this.newList);
    }

    // This method does the heavy lifting of pushing the work to the background thread
    private void updateListInternal(final List<T> newItems) {
        final List<T> oldItems = new ArrayList<>(list);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new BaseDiffUtil<T>(oldItems, newItems), true);
                appExecutors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        applyDiffResult(newItems, diffResult);
                    }
                });
            }
        };

        appExecutors.diskIO().execute(runnable);
    }

    // This method is called when the background work is done
    private void applyDiffResult(List<T> newItems,
                                 DiffUtil.DiffResult diffResult) {
        pendingUpdates.remove(newItems);
        dispatchUpdates(newItems, diffResult);
        if (pendingUpdates.size() > 0) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (pendingUpdates.size() > 0) {
                        List<T> latest = pendingUpdates.pop();
                        pendingUpdates.clear();
                        updateListInternal(latest);
                    }
                }
            }, 1000);
        }
    }

    // This method does the work of actually updating the backing data and notifying the storesCategoriesAdapter
    private void dispatchUpdates(List<T> newItems,
                                 DiffUtil.DiffResult diffResult) {
        list.clear();
        list.addAll(newItems);
        diffResult.dispatchUpdatesTo(this);
    }

    public void setItemSelectedListener(@Nullable ItemSelectedListener<T> itemSelectedListener) {
        this.itemSelectedListener = itemSelectedListener;
    }

    public List<T> getNewList() {
        return newList == null ? list : newList;
    }

    public interface ItemSelectedListener<T> {

        void onItemSelected(T listItem);
    }

    public static abstract class ItemSelectedListenerConstrainedClick<T> implements ItemSelectedListener<T> {
        private long lastClickTime = 0;

        private void setLastClickTime(long time) {
            lastClickTime = time;
        }

        public boolean shouldAllowTheClick() {
            long now = System.currentTimeMillis();
            if (lastClickTime != 0 && now - lastClickTime < Constant.CLICK_TIME_INTERVAL) {
                return false;
            } else {
                setLastClickTime(now);
                return true;
            }
        }
    }
}

