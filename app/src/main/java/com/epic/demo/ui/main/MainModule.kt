package com.epic.demo.ui.main


import com.daruma.demo.ui.main.MainContract
import com.epic.demo.ui.main.start.ImageContract
import com.epic.demo.ui.main.start.ImagePresenter
import com.epic.demo.di.FragmentScoped
import com.epic.demo.ui.main.start.ImageFragment
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainModule {
    
    @Binds
    abstract fun mainPresenter(mainPresenter: MainPresenter): MainContract.Presenter

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun wishFragment(): ImageFragment

    @Binds
    abstract fun wishPresenter(wishPresenter: ImagePresenter): ImageContract.Presenter


    @Module
    companion object {
    
    }
}