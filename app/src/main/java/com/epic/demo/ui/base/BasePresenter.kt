package com.epic.demo.ui.base

interface BasePresenter<T> {
    
    fun start()
    
    fun takeView(view: T)
    
}
