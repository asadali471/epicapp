package com.epic.demo.ui.main

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.daruma.demo.events.*
import com.epic.demo.repo.SharedPref
import com.epic.demo.ui.base.BaseActivity
import com.daruma.demo.ui.main.MainContract
import com.epic.demo.R
import com.epic.demo.databinding.ActivityMainBinding
import com.epic.demo.handler.FragmentHandler
import com.epic.demo.ui.base.BaseFragment
import com.epic.demo.ui.main.start.ImageFragment
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject


class MainActivity : BaseActivity(), MainContract.View {
    
    @Inject
    lateinit var eventBus: EventBus
    
    private lateinit var binding: ActivityMainBinding
    
    @Inject
    lateinit var mPresenter: MainContract.Presenter

    @Inject
    lateinit var pref: SharedPref
    
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        
        init()
    }
    
    override fun init() {

        mPresenter.takeView(this)

        setUpListeners()

        openImageFragment()
    }


    private fun clearFragmentFromFragmentContainer() {
        (supportFragmentManager.findFragmentById(R.id.fragmentContainerFullScreen) as BaseFragment?)?.let {
            if (it !is IOBackPressed || !it.onBackPressed()) {
                super.onBackPressed()
            }
        }
    }
    
    override fun setUpListeners() {

    }

    private fun openImageFragment() {

        FragmentHandler.addFragmentWithAnimation(this,
            ImageFragment.newInstance(),
            R.id.fragmentContainerFullScreen,
            false,
            ImageFragment::class.java.simpleName,
            R.anim.in_from_bottom,
            R.anim.out_to_bottom,
            R.anim.in_from_bottom,
            R.anim.out_to_bottom,
            false)

    }

    override fun isActive(): Boolean {
        return !isFinishing && !isDestroyed
    }
    
    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onBackPressed() {
        super.onBackPressed()

    }

    companion object {
        var TAG = MainActivity::class.java.simpleName
    }
}
