package com.epic.demo.ui.main

import com.daruma.demo.ui.main.MainContract
import javax.inject.Inject

class MainPresenter @Inject constructor() :
    MainContract.Presenter {

    private lateinit var mView: MainContract.View
    
    
    override fun takeView(view: MainContract.View) {
        mView = view
    }
    
    override fun start() {

    }
}