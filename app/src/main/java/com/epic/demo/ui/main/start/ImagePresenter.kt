package com.epic.demo.ui.main.start

import com.epic.demo.repo.EpicDataSource
import com.epic.demo.EpicApp
import javax.inject.Inject

class ImagePresenter @Inject constructor(val app: EpicApp, private val epicRepo: EpicDataSource) :
    ImageContract.Presenter {
    
    lateinit var mView: ImageContract.View

    override fun start() {
        loadImageFromEpic()
    }

    private fun loadImageFromEpic() {
        epicRepo.loadEpicImage(object: EpicDataSource.FetchImageCallback {
            override fun onImageLoadFailed(message: String) {
                mView.takeIf { it.isActive() }?.let {
                    it.showImageFailed(message)
                }
            }

            override fun onImageLOad(image: String) {
                mView.takeIf { it.isActive() }?.let {
                    it.showImage(image)
                }
            }
        })

    }

    override fun takeView(view: ImageContract.View) {
        mView = view
    }
}