package com.epic.demo.ui.main.start

import com.epic.demo.ui.base.BasePresenter
import com.daruma.demo.ui.base.BaseView

class ImageContract {
    
    interface Presenter : BasePresenter<View> {


    }
    
    interface View : BaseView<Presenter> {
        fun showImage(image: String)
        fun showImageFailed(message: String)

    }
}