package com.epic.demo.ui.base

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.epic.demo.R
import com.epic.demo.utils.Util

import dagger.android.support.DaggerAppCompatActivity


/*
 *   This class is a base class for all the activities in this project.
 * */
abstract class BaseActivity : DaggerAppCompatActivity() {

    var isOnPauseCalled: Boolean = false

    protected abstract fun init()

    protected abstract fun setUpListeners()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    fun intentActivity(
            baseActionBarFragmentActivity: Context,
            selectUserRoleActivityClass: Class<*>,
            slide_in_anim: Int,
            slide_out_anim: Int,
            flags: Int,
            bundle: Bundle?) {
        val openSignup = Intent(baseActionBarFragmentActivity,
                                selectUserRoleActivityClass)
        openSignup.flags = flags
        openSignup.putExtra("activity", baseActionBarFragmentActivity.javaClass.simpleName)
        if (bundle != null)
            openSignup.putExtras(bundle)
        startActivity(openSignup)
        overridePendingTransition(
                slide_in_anim, slide_out_anim)

    }

    fun startActivityWithTopBottomTransition(intent: Intent) {
        super.startActivityForResult(intent, ACTIVITY_TOP_BOTTOM_TRANSLATION_REQUEST_CODE)
        overridePendingTransition(R.anim.in_from_top, R.anim.out_to_bottom)
    }

    fun startActivityWithLeftRightTransition(intent: Intent) {
        super.startActivityForResult(intent, ACTIVITY_LEFT_RIGHT_TRANSLATION_REQUEST_CODE)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ACTIVITY_LEFT_RIGHT_TRANSLATION_REQUEST_CODE) {
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        } else if (requestCode == ACTIVITY_TOP_BOTTOM_TRANSLATION_REQUEST_CODE) {
            overridePendingTransition(R.anim.in_from_bottom, R.anim.out_to_top)
        }
    }

    override fun onPause() {
        super.onPause()
        isOnPauseCalled = true
    }

    override fun onResume() {
        super.onResume()
        isOnPauseCalled = false
    }

    fun popBackStack() {
        //To hide keyboard
        Util.hideKeyboard(this)
        if (supportFragmentManager.backStackEntryCount == 0) {
            finish()
        } else {
            supportFragmentManager.popBackStack()
        }
    }

    fun popBackStackImmediate() {
        //To hide keyboard
        Util.hideKeyboard(this)
        if (supportFragmentManager.backStackEntryCount == 0) {
            finish()
        } else {
            supportFragmentManager.popBackStackImmediate()
        }
    }

    companion object {

        val ACTIVITY_LEFT_RIGHT_TRANSLATION_REQUEST_CODE = 500

        val ACTIVITY_TOP_BOTTOM_TRANSLATION_REQUEST_CODE = 501

        private val TAG = BaseActivity::class.java.simpleName
    }
    
    interface BackStackUser {
        fun onBackPressed() : Boolean
    }
}
