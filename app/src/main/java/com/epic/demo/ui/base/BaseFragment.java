package com.epic.demo.ui.base;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.epic.demo.utils.AppLog;
import com.epic.demo.utils.Util;

import dagger.android.support.DaggerFragment;


public abstract class BaseFragment extends DaggerFragment {

    private static final String TAG = BaseFragment.class.getSimpleName();

    public BaseActivity mBaseActivity;

    private boolean doLog = true;

    public BaseFragment() {
        // Required empty public constructor
    }

    protected abstract void init();

    protected abstract void setupListeners();

    public void startActivity(Intent intent) {
        mBaseActivity.startActivityWithLeftRightTransition(intent);
    }

    public BaseFragment attachArguments(Bundle bundle) {
        if (bundle != null) {
            setArguments(bundle);
        }
        return this;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (doLog) {
            AppLog.INSTANCE.i(TAG, "onAttach: " + this.getClass().getName());
        }
        mBaseActivity = (BaseActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (doLog)
            AppLog.INSTANCE.i(TAG, "onCreate: " + this.getClass().getName());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (doLog)
            AppLog.INSTANCE.i(TAG, "onCreateView: " + this.getClass().getName());
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (doLog)
            AppLog.INSTANCE.i(TAG, "onResume: " + this.getClass().getName());
    }

    public void showMessage(String message) {
        Toast.makeText(mBaseActivity,message,Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (doLog)
            AppLog.INSTANCE.i(TAG, "onPause: " + this.getClass().getName());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (doLog)
            AppLog.INSTANCE.i(TAG, "onDestroyView: " + this.getClass().getName());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (doLog)
            AppLog.INSTANCE.i(TAG, "onDestroy: " + this.getClass().getName());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (doLog)
            AppLog.INSTANCE.i(TAG, "onDetach: " + this.getClass().getName());
    }

    public void showSimpleToast(String text) {
        Toast.makeText(mBaseActivity, text, Toast.LENGTH_SHORT).show();
    }

    public void activateHideKeyboardUponTouchingScreen(final View view) {
        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    Util.hideKeyboard(getActivity());
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                activateHideKeyboardUponTouchingScreen(innerView);
            }
        }
    }

    public interface ToolbarInteraction {

        void setUpToolbar();
    }
}