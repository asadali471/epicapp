package com.daruma.demo.ui.main

import com.epic.demo.ui.base.BasePresenter
import com.daruma.demo.ui.base.BaseView

class MainContract {
    
    interface Presenter : BasePresenter<View> {
    }
    
    interface View : BaseView<Presenter> {

    }
}