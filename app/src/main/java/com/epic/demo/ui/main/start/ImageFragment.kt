package com.epic.demo.ui.main.start


import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import com.epic.demo.R
import com.epic.demo.databinding.FragmentImageBinding
import com.epic.demo.executors.AppExecutors
import com.epic.demo.repo.SharedPref
import com.epic.demo.ui.base.BaseFragment
import javax.inject.Inject


private const val ARG_WISH = "ARG_WISH"

class ImageFragment : BaseFragment(), ImageContract.View {

    private lateinit var binding: FragmentImageBinding

    @Inject
    lateinit var appExecutors: AppExecutors

    @Inject
    lateinit var mPresenter: ImageContract.Presenter

    @Inject
    lateinit var sharedPref: SharedPref


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_image, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
    }


    override fun init() {

        mPresenter.takeView(this)

        setupListeners()

        mPresenter.start()

    }

    override fun setupListeners() {

    }

    companion object {
        @JvmStatic
        fun newInstance() = ImageFragment().apply {
            arguments = Bundle().apply {

            }
        }
    }

    override fun showImage(image: String) {
        binding.image = image
        binding.executePendingBindings()

    }

    override fun showImageFailed(message: String) {
        showMessage(message)
    }

    override fun isActive(): Boolean {
        return isAdded
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    override fun onDetach() {
        super.onDetach()
    }
}
