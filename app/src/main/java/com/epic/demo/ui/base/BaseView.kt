package com.daruma.demo.ui.base

interface BaseView<T> {
    
    fun isActive(): Boolean
    
}
